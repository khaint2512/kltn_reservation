package com.example.reservation.controller;

import com.example.reservation.dto.ReservationDetailsRequestDto;
import com.example.reservation.dto.ReservationRequestDto;
import com.example.reservation.dto.ReservationResponseDto;
import com.example.reservation.model.Reservation;
import com.example.reservation.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ReservationController {

    ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @RequestMapping(value = "/reservation/add", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity addReservation(@RequestBody ReservationRequestDto reservationRequestDto) {
        ReservationResponseDto reservationResponseDto = reservationService.addReservation(reservationRequestDto);

        ResponseEntity responseEntity = new ResponseEntity( reservationResponseDto ,HttpStatus.OK);
        return  responseEntity;
    }

    @GetMapping(value = "/reservation/status", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity readState(@RequestParam(name="reservation_code") String code){
        ReservationRequestDto reservationRequestDto = new ReservationRequestDto();
        reservationRequestDto.setUnique_code(code);
        ReservationResponseDto reservationResponseDto = reservationService.readState(reservationRequestDto);
        ResponseEntity responseEntity = new ResponseEntity(reservationResponseDto, HttpStatus.OK);
        return responseEntity;
    }

    @PutMapping(value = "/reservation", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity editReservation(@RequestBody ReservationRequestDto reservationRequestDto) {
        ReservationResponseDto reservationResponseDto = reservationService.editReservation(reservationRequestDto);
        ResponseEntity responseEntity = new ResponseEntity(reservationResponseDto, HttpStatus.OK);
        return responseEntity;
    }

    @DeleteMapping(value = "/reservation", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity cancelReservation(@RequestBody ReservationRequestDto reservationRequestDto) {
        ReservationResponseDto reservationResponseDto = reservationService.cancelReservation(reservationRequestDto);
        ResponseEntity responseEntity = new ResponseEntity(reservationResponseDto, HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping(value = "reservation/{state}/get_list", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity listReservation(@PathVariable("state") int state){
        List<Reservation> list = reservationService.listReservation(state);
        ResponseEntity responseEntity = new ResponseEntity(list, HttpStatus.OK);
        return responseEntity;
    }

    @PutMapping(value = "/reservation/accept", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity acceptReservation(@RequestBody ReservationRequestDto reservationRequestDto) {
        ReservationResponseDto reservationResponseDto = reservationService.acceptReservation(reservationRequestDto);
        ResponseEntity responseEntity = new ResponseEntity(reservationResponseDto, HttpStatus.OK);
        return responseEntity;
    }

    @PutMapping(value = "/reservation/decline", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity declineReservation(@RequestBody ReservationRequestDto reservationRequestDto) {
        ReservationResponseDto reservationResponseDto = reservationService.declineReservation(reservationRequestDto);
        ResponseEntity responseEntity = new ResponseEntity(reservationResponseDto, HttpStatus.OK);
        return responseEntity;
    }

    @PutMapping(value = "/reservation/arrived", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity customerArrived(@RequestBody ReservationRequestDto reservationRequestDto){
        ReservationResponseDto reservationResponseDto = reservationService.customerArrived(reservationRequestDto);
        ResponseEntity responseEntity = new ResponseEntity(reservationResponseDto, HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping(value = "/reservation/{date}/statistic", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity statistic(@PathVariable("date") int reservationDatetime) {
        List<Reservation> reservationList = reservationService.statistic(reservationDatetime);
        ResponseEntity responseEntity = new ResponseEntity(reservationList, HttpStatus.OK);
        return responseEntity;
    }
}
