package com.example.reservation.controller;

import com.example.reservation.dto.DayOffRequestDto;
import com.example.reservation.dto.DayOffResponseDto;
import com.example.reservation.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ShopController {

    ShopService shopService;

    @Autowired
    public ShopController (ShopService shopService){
        this.shopService = shopService;
    }

    @PutMapping(value = "/shop/set_day_off", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity setDayOff(@RequestBody DayOffRequestDto dayOffRequestDto){
        DayOffResponseDto dayOffResponseDto = shopService.setDayOff(dayOffRequestDto);
        ResponseEntity responseEntity = new ResponseEntity(dayOffResponseDto, HttpStatus.OK);
        return responseEntity;
    }
}
