package com.example.reservation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class, WebMvcAutoConfiguration.class })

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.example.reservation.repository")
class RMDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(RMDemoApplication.class, args);
    }

}
