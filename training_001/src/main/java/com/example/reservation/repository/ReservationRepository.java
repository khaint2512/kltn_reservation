package com.example.reservation.repository;

import com.example.reservation.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, String> {
    Reservation findByUid (String uid);
    Reservation findByUniqueCode (String unique_code);
    List<Reservation> findByState (int state);
    String FIND_RESERVATION_DATETIME = " SELECT * FROM p_reservation WHERE reservation_datetime = '1593750780' ";

    @Query(value = FIND_RESERVATION_DATETIME, nativeQuery = true)
    List<Reservation> findAll();
}
