package com.example.reservation.repository;

import com.example.reservation.model.Table;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TableRepository extends JpaRepository<Table, String> {
    Table findByUid (String uid);
}
