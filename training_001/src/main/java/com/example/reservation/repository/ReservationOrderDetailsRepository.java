package com.example.reservation.repository;

import com.example.reservation.model.ReservationOrderDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReservationOrderDetailsRepository extends JpaRepository<ReservationOrderDetails, String> {
    List<ReservationOrderDetails> findByUid (String uid);
}
