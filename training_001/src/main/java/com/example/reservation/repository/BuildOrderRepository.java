//package com.example.rmdemo.repository;
//
//import com.example.rmdemo.dto.BuildOrderDTO;
//import com.example.rmdemo.dto.OrderDTO;
//import org.springframework.data.mongodb.repository.MongoRepository;
//
//import java.util.List;
//
//public interface BuildOrderRepository extends MongoRepository<BuildOrderDTO, String> {
//    BuildOrderDTO findByTable(String table);
//    BuildOrderDTO findByOrderDTOS(List<OrderDTO> orderDTOS);
//    BuildOrderDTO findByTotalQuantity(String totalQuantity);
//    BuildOrderDTO findByTotalPrice (String totalPrice);
//}
