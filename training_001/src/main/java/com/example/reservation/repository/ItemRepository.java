//package com.example.rmdemo.repository;
//
//import org.springframework.data.mongodb.repository.MongoRepository;
//
//import java.util.List;
//
//public interface ItemRepository extends MongoRepository<Item, String> {
//
//    Item findBy_id(String id);
//
//    Item findByName(String name);
//
//    List<Item> findAllByCategoryId(String categoryId);
//
//    List<Item> findByNameLike(String name);
//}
