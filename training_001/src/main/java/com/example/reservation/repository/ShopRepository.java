package com.example.reservation.repository;

import com.example.reservation.model.Shop;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShopRepository extends JpaRepository<Shop, String> {
    Shop findByUid (String uid);
}
