package com.example.reservation.model;

import com.example.reservation.dto.ReservationRequestDto;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity(name = "p_reservation")
@javax.persistence.Table(name = "p_reservation")
public class Reservation {
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String uid;

    private int table_id;

    private int shop_id;
    private int state;
    private String customer_name;
    private String phone_number;
    private String email;
    private int reservation_datetime;
    private int person_number;
    private String item_name;
    private int item_number;
    private String note;
    private int created;
    private int modified;
    private String uniqueCode;
    private String reason;
    private int canceled;
//    private ReservationRequestDto reservationRequestDto;
//    @OneToMany(mappedBy = "reservation_id", cascade = CascadeType.ALL)
//    private Set<ReservationOrder> ReservationOrders;

    public Reservation() {
    }

    public Reservation(ReservationRequestDto reservationRequestDto){
        this.shop_id = getShop_id();
        this.table_id = getTable_id();
        this.state = getState();
        this.customer_name = reservationRequestDto.getCustomer_name();
        this.phone_number = reservationRequestDto.getPhone_number();
        this.email = reservationRequestDto.getEmail();
        this.reservation_datetime = reservationRequestDto.getReservation_datetime();
        this.person_number = reservationRequestDto.getPerson_number();
        this.note = reservationRequestDto.getNote();
        this.uniqueCode = reservationRequestDto.getCode();
        this.created = reservationRequestDto.getCreated();
        this.reason = reservationRequestDto.getReason();
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getTable_id() {
        return table_id;
    }

    public void setTable_id(int table_id) {
        this.table_id = table_id;
    }

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getReservation_datetime() {
        return reservation_datetime;
    }

    public void setReservation_datetime(int reservation_datetime) {
        this.reservation_datetime = reservation_datetime;
    }

    public int getPerson_number() {
        return person_number;
    }

    public void setPerson_number(int person_number) {
        this.person_number = person_number;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public int getItem_number() {
        return item_number;
    }

    public void setItem_number(int item_number) {
        this.item_number = item_number;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public int getModified() {
        return modified;
    }

    public void setModified(int modified) {
        this.modified = modified;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String unique_code) {
        this.uniqueCode = unique_code;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getCanceled() {
        return canceled;
    }

    public void setCanceled(int canceled) {
        this.canceled = canceled;
    }

//    public ReservationRequestDto getReservationRequestDto() {
//        return reservationRequestDto;
//    }
//
//    public void setReservationRequestDto(ReservationRequestDto reservationRequestDto) {
//        this.reservationRequestDto = reservationRequestDto;
//    }
//


    //    public Set<ReservationOrder> getReservationOrders() {
//        return ReservationOrders;
//    }
//
//    public void setReservationOrders(Set<ReservationOrder> reservationOrders) {
//        this.ReservationOrders = reservationOrders;
//    }
}
