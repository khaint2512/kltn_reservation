package com.example.reservation.model;

public enum ReservationState {
    PENDING(1),
    ARRIVED(2),
    CANCELED(3),
    ACCEPTED(4),
    DECLINED(5),
    ACTIVE(6);

    private int id;
    private ReservationState(int id) {
        this.id = id;
    }
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
