package com.example.reservation.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity(name = "p_reservation_order_details")
//@Table(name = "p_reservation_order_details")
public class ReservationOrderDetails {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String uid;
    private int state;
    private int reservation_id;
    private int item_id;
    private String item_name;
    private String item_abbreviation;
    private int tax_inclusive_f;
    private int price;
    private int price_tax_in;
    private int discount_type;
    private int discount_value;
    private int discount;
    private int order_number;
    private int subtotal;
    private int tax;
    private int total;
    private int item_number;
    private String details;
    private int created;
    private int modified;

    public ReservationOrderDetails(){

    }

    public ReservationOrderDetails(String item_name, int item_number, int item_id){
        this.item_name = item_name;
        this.item_number = item_number;
        this.item_id = item_id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getReservation_id() {
        return reservation_id;
    }

    public void setReservation_id(int reservation_id) {
        this.reservation_id = reservation_id;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_abbreviation() {
        return item_abbreviation;
    }

    public void setItem_abbreviation(String item_abbreviation) {
        this.item_abbreviation = item_abbreviation;
    }

    public int getTax_inclusive_f() {
        return tax_inclusive_f;
    }

    public void setTax_inclusive_f(int tax_inclusive_f) {
        this.tax_inclusive_f = tax_inclusive_f;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPrice_tax_in() {
        return price_tax_in;
    }

    public void setPrice_tax_in(int price_tax_in) {
        this.price_tax_in = price_tax_in;
    }

    public int getDiscount_type() {
        return discount_type;
    }

    public void setDiscount_type(int discount_type) {
        this.discount_type = discount_type;
    }

    public int getDiscount_value() {
        return discount_value;
    }

    public void setDiscount_value(int discount_value) {
        this.discount_value = discount_value;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getOrder_number() {
        return order_number;
    }

    public void setOrder_number(int order_number) {
        this.order_number = order_number;
    }

    public int getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(int subtotal) {
        this.subtotal = subtotal;
    }

    public int getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getItem_number() {
        return item_number;
    }

    public void setItem_number(int item_number) {
        this.item_number = item_number;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public int getModified() {
        return modified;
    }

    public void setModified(int modified) {
        this.modified = modified;
    }
}
