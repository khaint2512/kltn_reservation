package com.example.reservation.model;

import com.example.reservation.dto.DayOffRequestDto;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "p_shop")
//@Table(name = "p_shop")
public class Shop {
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String uid;
    private int state;
    private int owner_id;
    private String name;
    private String details;
    private int day_off;
    private int created;
    private int modified;
    @OneToMany(mappedBy = "shop_id", cascade = CascadeType.ALL)
    private Set<Reservation> Reservations;

    public Shop(){
    }

    public Shop(DayOffRequestDto dayOffRequestDto){
        this.uid = dayOffRequestDto.getUid();
        this.state = dayOffRequestDto.getState();
        this.name = dayOffRequestDto.getName();
        this.day_off = dayOffRequestDto.getDay_off();
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(int owner_id) {
        this.owner_id = owner_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getDay_off() {
        return day_off;
    }

    public void setDay_off(int day_off) {
        this.day_off = day_off;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public int getModified() {
        return modified;
    }

    public void setModified(int modified) {
        this.modified = modified;
    }

    public Set<Reservation> getReservations() {
        return Reservations;
    }

    public void setReservations(Set<Reservation> reservations) {
        this.Reservations = reservations;
    }
}
