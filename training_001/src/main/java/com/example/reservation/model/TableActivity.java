package com.example.reservation.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "p_table_activity")
@javax.persistence.Table(name = "p_table_activity")
public class TableActivity {
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String uid;
//    @ManyToOne
//    @JoinColumn(name = "table_id")
//    private Table table_id;
    private int state;
    private int subtotal;
    private int tax;
    private int total;
    private int customer_number;
    private String details;
    private int timer_start;
    private int timer_duration;
    private int created;
    private int modified;
    @OneToMany(mappedBy = "table_activity_id", cascade = CascadeType.ALL)
    private Set<Order> Orders;

    public TableActivity(){
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

//    public Table getTable_id() {
//        return table_id;
//    }
//
//    public void setTable_id(Table table_id) {
//        this.table_id = table_id;
//    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(int subtotal) {
        this.subtotal = subtotal;
    }

    public int getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCustomer_number() {
        return customer_number;
    }

    public void setCustomer_number(int customer_number) {
        this.customer_number = customer_number;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getTimer_start() {
        return timer_start;
    }

    public void setTimer_start(int timer_start) {
        this.timer_start = timer_start;
    }

    public int getTimer_duration() {
        return timer_duration;
    }

    public void setTimer_duration(int timer_duration) {
        this.timer_duration = timer_duration;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public int getModified() {
        return modified;
    }

    public void setModified(int modified) {
        this.modified = modified;
    }

    public Set<Order> getOrders() {
        return Orders;
    }

    public void setOrders(Set<Order> orders) {
        this.Orders = orders;
    }
}
