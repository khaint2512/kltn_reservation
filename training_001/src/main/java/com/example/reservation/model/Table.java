package com.example.reservation.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;

@Entity
@javax.persistence.Table(name = "p_table")
public class Table {
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String uid;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "shop_id")
    private Shop shop_id;
    private int state;
    private String name;
    private String description;
    private int sort_order;
    private int created;
    private int modified;
    private int id;

    public Table(){

    }

    public Table(String uid){
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public Shop getShop_id() {
        return shop_id;
    }

    public void setShop_id(Shop shop_id) {
        this.shop_id = shop_id;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSort_order() {
        return sort_order;
    }

    public void setSort_order(int sort_order) {
        this.sort_order = sort_order;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public int getModified() {
        return modified;
    }

    public void setModified(int modified) {
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
