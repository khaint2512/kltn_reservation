package com.example.reservation.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "p_item")
//@Table(name = "p_item")
public class Item {
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String uid;
    private String item_detail_uid;
    private int state;
    private String name;
    private int price;
    private int price_tax_in;
    private int cost;
    private int tax;
    private int tax_rate;
    private int tax_inclusive_f;
    private String details;
    private int stock;
    private String description;
    private int background_type;
    private int created;
    private int modified;
    @OneToMany(mappedBy = "item_id", cascade = CascadeType.ALL)
    private Set<ReservationOrderDetails> ReservationOrderDetails;

    public Item(){

    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getItem_detail_uid() {
        return item_detail_uid;
    }

    public void setItem_detail_uid(String item_detail_uid) {
        this.item_detail_uid = item_detail_uid;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPrice_tax_in() {
        return price_tax_in;
    }

    public void setPrice_tax_in(int price_tax_in) {
        this.price_tax_in = price_tax_in;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }

    public int getTax_rate() {
        return tax_rate;
    }

    public void setTax_rate(int tax_rate) {
        this.tax_rate = tax_rate;
    }

    public int getTax_inclusive_f() {
        return tax_inclusive_f;
    }

    public void setTax_inclusive_f(int tax_inclusive_f) {
        this.tax_inclusive_f = tax_inclusive_f;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getBackground_type() {
        return background_type;
    }

    public void setBackground_type(int background_type) {
        this.background_type = background_type;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public int getModified() {
        return modified;
    }

    public void setModified(int modified) {
        this.modified = modified;
    }

    public Set<ReservationOrderDetails> getReservationOrderDetails() {
        return ReservationOrderDetails;
    }

    public void setReservationOrderDetails(Set<ReservationOrderDetails> reservationOrderDetails) {
        this.ReservationOrderDetails = reservationOrderDetails;
    }
}
