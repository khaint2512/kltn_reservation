package com.example.reservation.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity(name = "p_reservation_order")
//@Table(name = "p_reservation_order")
public class ReservationOrder {
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String uid;
    @ManyToOne
    @JoinColumn(name = "reservation_id")
    private Reservation reservation_id;
    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order_id;

    public ReservationOrder(){
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Reservation getReservation_id() {
        return reservation_id;
    }

    public void setReservation_id(Reservation reservation_id) {
        this.reservation_id = reservation_id;
    }

    public Order getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Order order_id) {
        this.order_id = order_id;
    }
}
