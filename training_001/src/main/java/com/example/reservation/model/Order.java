package com.example.reservation.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "p_order")
//@Table(name = "p_order")
public class Order {
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String uid;
    @ManyToOne
    @JoinColumn(name="table_activity_id")
    private TableActivity table_activity_id;
    private int state;
    private int created;
    private int modified;
    @OneToMany(mappedBy = "order_id", cascade = CascadeType.ALL)
    private Set<ReservationOrder> ReservationOrders;

    public Order(){
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public TableActivity getTable_activity_id() {
        return table_activity_id;
    }

    public void setTable_activity_id(TableActivity table_activity_id) {
        this.table_activity_id = table_activity_id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public int getModified() {
        return modified;
    }

    public void setModified(int modified) {
        this.modified = modified;
    }

    public Set<ReservationOrder> getReservationOrders() {
        return ReservationOrders;
    }

    public void setReservationOrders(Set<ReservationOrder> reservationOrders) {
        this.ReservationOrders = reservationOrders;
    }
}
