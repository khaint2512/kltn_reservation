//package com.example.rmdemo.service;
//
//import com.example.rmdemo.dto.BuildOrderDTO;
//import com.example.rmdemo.dto.OrderDTO;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//
//import java.util.List;
//
//public interface OrderService {
//    Iterable <Order> findAll();
//
//    List<OrderDTO> findListItem(String itemName);
//
//    List<Order> search(String id);
//
//    List<Order> orderList(String itemId, String item, String quantity, String price);
//
//    Iterable <Table> listTable(String name);
//
//    Iterable <Item> listItem(String name);
//
//    Table findTableById (String id);
//
//    Table findTable(String name);
//
//    Item findItemById(String id);
//
//    Item findItem(String name);
//
//    Order findOrderById(String id);
//
//    String addOrder (BuildOrderDTO data, RedirectAttributes redirect);
////    String addOrder(BuildOrderDTO data, String tableName, List<OrderDTO> orderList, String totalQuantity, String totalPrice, boolean isPaid, RedirectAttributes redirect);
////    String addOrder(String shopId, String tableId, String tableName, String itemId, String itemName, String itemPrice, String quantity, String price, boolean isPaid, RedirectAttributes redirect);
//    String editOrder(String tableId, String itemId, String id, String newQuantity, String newPrice, boolean newPaid,RedirectAttributes redirect);
//
//    String deleteOrder(String tableId, String itemId, String id, RedirectAttributes redirect);
//
//    String paidOrder(String tableId, String itemId, String id, RedirectAttributes redirect);
//
//    String unpaidOrder(String tableId, String itemId, String id, RedirectAttributes redirect);
//}
