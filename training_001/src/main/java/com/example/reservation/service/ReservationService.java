package com.example.reservation.service;

import com.example.reservation.dto.ReservationDetailsRequestDto;
import com.example.reservation.dto.ReservationRequestDto;
import com.example.reservation.dto.ReservationResponseDto;
import com.example.reservation.model.Reservation;
import com.example.reservation.model.Shop;
import com.example.reservation.model.Table;
import java.util.List;

public interface ReservationService {

    Iterable <Reservation> findAll();

    Table findTableByUid(String uid);

    Shop findShopByUid(String uid);

    ReservationResponseDto addReservation(ReservationRequestDto reservationRequestDto);

    ReservationResponseDto readState(ReservationRequestDto reservationRequestDto);

    ReservationResponseDto editReservation(ReservationRequestDto reservationRequestDto);

    ReservationResponseDto cancelReservation(ReservationRequestDto reservationRequestDto);

    List<Reservation> listReservation(int state);

    ReservationResponseDto acceptReservation(ReservationRequestDto reservationRequestDto);

    ReservationResponseDto declineReservation(ReservationRequestDto reservationRequestDto);

    ReservationResponseDto customerArrived(ReservationRequestDto reservationRequestDto);

//    public List<Reservation> reservationList();
    List<Reservation> statistic(int reservationDatetime);
}
