package com.example.reservation.service;

import com.example.reservation.dto.DayOffRequestDto;
import com.example.reservation.dto.DayOffResponseDto;
import com.example.reservation.model.Shop;

public interface ShopService {

    Iterable <Shop> findAll();

    DayOffResponseDto setDayOff(DayOffRequestDto dayOffRequestDto);
}
