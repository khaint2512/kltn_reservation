package com.example.reservation.service.implement;

import com.example.reservation.dto.DayOffRequestDto;
import com.example.reservation.dto.DayOffResponseDto;
import com.example.reservation.model.ReservationState;
import com.example.reservation.model.Shop;
import com.example.reservation.repository.ShopRepository;
import com.example.reservation.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShopServiceImpl implements ShopService {

    ShopRepository shopRepository;

    @Autowired
    public ShopServiceImpl (ShopRepository shopRepository){
        this.shopRepository = shopRepository;
    }

    @Override
    public Iterable<Shop> findAll() {
        return shopRepository.findAll();
    }

    @Override
    public DayOffResponseDto setDayOff(DayOffRequestDto dayOffRequestDto) {
        Shop shop = shopRepository.findByUid(dayOffRequestDto.getUid());
        shop.setState(ReservationState.ACTIVE.getId());
        shop.setDay_off(dayOffRequestDto.getDay_off());
        shop.setModified((int)(System.currentTimeMillis() / 1000));
        shopRepository.save(shop);

        DayOffResponseDto dayOffResponseDto = new DayOffResponseDto();
        dayOffResponseDto.setName(shop.getName());
        dayOffResponseDto.setDay_off(shop.getDay_off());
        dayOffResponseDto.setModified(shop.getModified());
        return dayOffResponseDto;
    }
}
