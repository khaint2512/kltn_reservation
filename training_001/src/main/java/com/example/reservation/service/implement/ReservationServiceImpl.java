package com.example.reservation.service.implement;

import com.example.reservation.dto.ReservationDetailsRequestDto;
import com.example.reservation.dto.ReservationRequestDto;
import com.example.reservation.dto.ReservationResponseDto;
import com.example.reservation.model.*;
import com.example.reservation.repository.ReservationOrderDetailsRepository;
import com.example.reservation.repository.ReservationRepository;
import com.example.reservation.repository.ShopRepository;
import com.example.reservation.repository.TableRepository;
import com.example.reservation.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReservationServiceImpl implements ReservationService {

    ReservationRepository reservationRepository;
    TableRepository tableRepository;
    ShopRepository shopRepository;
    ReservationOrderDetailsRepository reservationOrderDetailsRepository;

    @Autowired
    public ReservationServiceImpl(ReservationRepository reservationRepository, TableRepository tableRepository, ShopRepository shopRepository, ReservationOrderDetailsRepository reservationOrderDetailsRepository) {
        this.reservationRepository = reservationRepository;
        this.tableRepository = tableRepository;
        this.shopRepository = shopRepository;
        this.reservationOrderDetailsRepository = reservationOrderDetailsRepository;
    }


    @Override
    public Iterable<Reservation> findAll() {
        return reservationRepository.findAll();
    }

    @Override
    public Table findTableByUid(String uid) {
        return tableRepository.findByUid(uid);
    }

    @Override
    public Shop findShopByUid(String uid) {
        return shopRepository.findByUid(uid);
    }

    @Override
    public ReservationResponseDto addReservation(ReservationRequestDto reservationRequestDto) {
        List<ReservationOrderDetails> reservationOrderDetails = reservationRequestDto.getReservationOrderDetails();
        Reservation reservation = new Reservation(reservationRequestDto);
        reservation.setUid(reservationRequestDto.getUid());
        reservation.setShop_id(reservationRequestDto.getShop_id());
        reservation.setState(ReservationState.PENDING.getId());
        for (ReservationOrderDetails reservationInfo : reservationOrderDetails){
            String item_name = reservationInfo.getItem_name();
            int item_number = reservationInfo.getItem_number();
            int item_id = reservationInfo.getItem_id();
            ReservationOrderDetails reservationOrderDetails1 = new ReservationOrderDetails(item_name, item_number, item_id);
            reservationOrderDetailsRepository.save(reservationOrderDetails1);
        }
        reservationRepository.save(reservation);
        ReservationResponseDto reservationResponseDto = new ReservationResponseDto();
        reservationResponseDto.setState(reservation.getState());
        reservationResponseDto.setUniqueCode(reservation.getUniqueCode());

        reservationResponseDto.setCreated(reservation.getCreated());

        return reservationResponseDto;
    }

    @Override
    public ReservationResponseDto readState(ReservationRequestDto reservationRequestDto) {
        Reservation reservation = reservationRepository.findByUniqueCode(reservationRequestDto.getUnique_code());

        ReservationResponseDto reservationResponseDto = new ReservationResponseDto();
        reservationResponseDto.setState(reservation.getState());
        reservationResponseDto.setUniqueCode(reservation.getUniqueCode());
        reservationResponseDto.setCreated(reservation.getCreated());
        reservationResponseDto.setModified(reservation.getModified());
        reservationResponseDto.setUid(reservation.getUid());
        return reservationResponseDto;
    }

    @Override
    public ReservationResponseDto editReservation(ReservationRequestDto reservationRequestDto) {
        List<ReservationOrderDetails> reservationOrderDetails = reservationRequestDto.getReservationOrderDetails();
        Reservation reservation = reservationRepository.findByUniqueCode(reservationRequestDto.getUnique_code());
//        reservation.setState(ReservationState.PENDING.getId());
        reservation.setCustomer_name(reservationRequestDto.getCustomer_name());
        reservation.setPhone_number(reservationRequestDto.getPhone_number());
        reservation.setEmail(reservationRequestDto.getEmail());
        reservation.setReservation_datetime(reservationRequestDto.getReservation_datetime());
        reservation.setPerson_number(reservationRequestDto.getPerson_number());

        for (ReservationOrderDetails reservationInfo : reservationOrderDetails){
            String item_name = reservationInfo.getItem_name();
            int item_number = reservationInfo.getItem_number();
            int item_id = reservationInfo.getItem_id();
            ReservationOrderDetails reservationOrderDetails1 = new ReservationOrderDetails(item_name, item_number, item_id);
            reservationOrderDetailsRepository.save(reservationOrderDetails1);
        }
        reservation.setNote(reservationRequestDto.getNote());
        reservation.setModified((int)(System.currentTimeMillis() / 1000));

        reservationRepository.save(reservation);
        ReservationResponseDto reservationResponseDto = new ReservationResponseDto();

        reservationResponseDto.setState(reservation.getState());
        reservationResponseDto.setUniqueCode(reservation.getUniqueCode());
        reservationResponseDto.setCreated(reservation.getCreated());
        reservationResponseDto.setModified(reservation.getModified());
        return reservationResponseDto;
    }

    @Override
    public ReservationResponseDto cancelReservation(ReservationRequestDto reservationRequestDto) {
        Reservation reservation = reservationRepository.findByUniqueCode(reservationRequestDto.getUnique_code());

        reservation.setReason(reservationRequestDto.getReason());
        reservation.setState(ReservationState.CANCELED.getId());
        reservation.setModified((int)(System.currentTimeMillis() / 1000));
        reservation.setCanceled((int)(System.currentTimeMillis() / 1000));

        reservationRepository.save(reservation);
        ReservationResponseDto reservationResponseDto = new ReservationResponseDto();
        reservationResponseDto.setState(reservation.getState());
        reservationResponseDto.setUniqueCode(reservation.getUniqueCode());
        reservationResponseDto.setCreated(reservation.getCreated());
        reservationResponseDto.setModified(reservation.getModified());
        reservationResponseDto.setCanceled(reservation.getCanceled());
        return reservationResponseDto;
    }

    @Override
    public List<Reservation> listReservation(int state) {
        List<Reservation> reservation = reservationRepository.findByState(ReservationState.PENDING.getId());

        return reservation;
    }

    @Override
    public ReservationResponseDto acceptReservation(ReservationRequestDto reservationRequestDto){
        Reservation reservation = reservationRepository.findByUid(reservationRequestDto.getUid());

        reservation.setState(ReservationState.ACCEPTED.getId());
        reservation.setModified((int)(System.currentTimeMillis() / 1000));

        reservationRepository.save(reservation);
        ReservationResponseDto reservationResponseDto = new ReservationResponseDto();

        reservationResponseDto.setState(reservation.getState());
        reservationResponseDto.setUniqueCode(reservation.getUniqueCode());
        reservationResponseDto.setCreated(reservation.getCreated());
        reservationResponseDto.setModified(reservation.getModified());
        return reservationResponseDto;
    }

    @Override
    public ReservationResponseDto declineReservation(ReservationRequestDto reservationRequestDto) {
        Reservation reservation = reservationRepository.findByUid(reservationRequestDto.getUid());

        reservation.setState(ReservationState.DECLINED.getId());
        reservation.setModified((int)(System.currentTimeMillis() / 1000));

        reservationRepository.save(reservation);

        ReservationResponseDto reservationResponseDto = new ReservationResponseDto();

        reservationResponseDto.setState(reservation.getState());
        reservationResponseDto.setUniqueCode(reservation.getUniqueCode());
        reservationResponseDto.setCreated(reservation.getCreated());
        reservationResponseDto.setModified(reservation.getModified());
        return reservationResponseDto;
    }

    @Override
    public ReservationResponseDto customerArrived(ReservationRequestDto reservationRequestDto) {
        Reservation reservation = reservationRepository.findByUid(reservationRequestDto.getUid());

        reservation.setState(ReservationState.ARRIVED.getId());
        reservation.setTable_id(2);
        reservation.setModified((int)(System.currentTimeMillis() / 1000));

        reservationRepository.save(reservation);

        ReservationResponseDto reservationResponseDto = new ReservationResponseDto();

        reservationResponseDto.setState(reservation.getState());
        reservationResponseDto.setUniqueCode(reservation.getUniqueCode());
        reservationResponseDto.setCreated(reservation.getCreated());
        reservationResponseDto.setModified(reservation.getModified());
        return reservationResponseDto;
    }

    @Override
    public List<Reservation> statistic(int reservationDatetime) {
        List<Reservation> reservation = reservationRepository.findAll();
        return reservation;
    }

    //    public List<Reservation> reservationList() {
//        reservationRepository.findAll();
//
//        return null;
//    }
}
