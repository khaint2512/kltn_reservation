//package com.example.rmdemo.service;
//
//import com.example.rmdemo.repository.ItemRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//public class ItemServiceImpl implements ItemService {
//
//    @Autowired
//    ItemRepository itemRepository;
//
//    @Override
//    public Iterable<Item> findAll() {
//        return itemRepository.findAll();
//    }
//
//    @Override
//    public List<Item> search(String name) {
//        return itemRepository.findByNameLike(name);
//    }
//}
