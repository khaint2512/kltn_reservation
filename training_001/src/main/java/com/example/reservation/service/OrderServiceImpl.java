//package com.example.rmdemo.service;
//
//import com.example.rmdemo.dto.BuildOrderDTO;
//import com.example.rmdemo.dto.OrderDTO;
//import com.example.rmdemo.repository.ItemRepository;
//import com.example.rmdemo.repository.ItemOrderRepository;
//import com.example.rmdemo.repository.BuildOrderRepository;
//import com.example.rmdemo.repository.OrderRepository;
//import com.example.rmdemo.repository.OrderDTORepository;
//import com.example.rmdemo.repository.TableRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//
//import java.util.List;
//
//@Service
//public class OrderServiceImpl implements OrderService {
//
//    @Autowired
//    OrderRepository orderRepository;
//
//    @Autowired
//    ItemRepository itemRepository;
//
//    @Autowired
//    BuildOrderRepository buildOrderRepository;
//
//    @Autowired
//    ItemOrderRepository itemOrderRepository;
//
//    @Autowired
//    OrderDTORepository orderDTORepository;
//
//    @Autowired
//    TableRepository tableRepository;
//
//    @Override
//    public Iterable<Order> findAll() {return orderRepository.findAll();}
//
//    @Override
//    public List<OrderDTO> findListItem(String itemName) {
//        return orderDTORepository.findAll();
//    }
//
//    @Override
//    public List<Order> search(String id) {
//        return orderRepository.findAll();
//    }
//
//    @Override
//    public List<Order> orderList(String itemId, String item, String quantity, String price) {
//        return orderRepository.findAll();
//    }
//
//    @Override
//    public Iterable<Table> listTable(String name) {
//        return tableRepository.findByNameLike(name);
//    }
//
//    @Override
//    public Iterable<Item> listItem(String name) {
//        return itemRepository.findByNameLike(name);
//    }
//
//    @Override
//    public Table findTableById(String id) {
//        return tableRepository.findBy_id(id);
//    }
//
//    @Override
//    public Table findTable(String name) {
//        return tableRepository.findByName(name);
//    }
//
//    @Override
//    public Item findItemById(String id) {
//        return itemRepository.findBy_id(id);
//    }
//
//    @Override
//    public Item findItem(String name) {
//        return itemRepository.findByName(name);
//    }
//
//    @Override
//    public Order findOrderById(String id) {
//        return orderRepository.findBy_id(id);
//    }
//
////    @Override
////    public String addOrder(String shopId, String tableId, String tableName, String itemId, String itemName, String itemPrice, String quantity, String price, boolean isPaid, RedirectAttributes redirect) {
////        Order order = new Order(shopId, tableId, tableName, itemId, itemName, itemPrice, quantity, price, isPaid);
////        orderRepository.save(order);
////        redirect.addFlashAttribute("successMessage", "Add Order successfull!");
////        return "redirect:/orders/view";
////    }
//
////    @Override
////    public String addOrder(BuildOrderDTO data, String tableName, List<OrderDTO> orderList, String totalQuantity, String totalPrice, boolean isPaid, RedirectAttributes redirect) {
////        tableName = data.getTable();
////
////        orderList = data.getOrderDTOS();
////        for (OrderDTO order : orderList) {
////            String itemId = order.getItemId();
////            String itemName = order.getItemName();
////            String quantity = order.getQuantity();
////            String price = order.getPrice();
////            ItemOrder itemOrder = new ItemOrder(itemId,itemName,quantity,price);
////            itemOrderRepository.save(itemOrder);
////        }
////        totalQuantity = data.getTotalQuantity();
////        totalPrice = data.getTotalPrice();
////        isPaid = data.isPaid();
////        Order order = new Order(tableName, totalQuantity, totalPrice, isPaid);
////        String redirectValue = "orders/view";
////        orderRepository.save(order);
////        redirect.addFlashAttribute("successMessage", "Add Order Success");
////        return redirectValue;
////    }
//
//    @Override
//    public String addOrder(BuildOrderDTO data, RedirectAttributes redirect) {
//        String table = data.getTable();
//        List<OrderDTO> orderDTOS = data.getOrderDTOS();
//        String totalQuantity = data.getTotalQuantity();
//        String totalPrice = data.getTotalPrice();
//        Boolean status = data.isPaid();
//
//        Order order = new Order(table, orderDTOS, totalQuantity, totalPrice, status);
////        BuildOrderDTO order = new BuildOrderDTO(table, orderList, totalQuantity, totalPrice, isPaid);
//        for (OrderDTO orderItem : orderDTOS) {
//            String itemId = orderItem.getItemId();
//            String itemName = orderItem.getItemName();
//            String quantity = orderItem.getQuantity();
//            String price = orderItem.getPrice();
//            ItemOrder itemOrder = new ItemOrder(itemId, itemName, quantity, price);
//            itemOrderRepository.save(itemOrder);
//        }
//        orderRepository.save(order);
//        redirect.addFlashAttribute("successMessage", "Add Order Success");
//        return "orders/view";
//    }
//
//
//    @Override
//    public String editOrder(String id, @ModelAttribute("currentTableId")  String tableId, @ModelAttribute("currentItemId") String itemId, String newQuantity, String newPrice, boolean newPaid, RedirectAttributes redirect) {
//        Order order = orderRepository.findBy_id(id);
////        order.setQuantity(newQuantity);
//        order.setPaid(newPaid);
//        orderRepository.save(order);
//        String redirectValue = "orders/view";
//        redirect.addFlashAttribute("successMessage", "Edit Order Success");
//        return redirectValue;
//    }
//
//    @Override
//    public String deleteOrder(String id, @ModelAttribute("currentTableId")  String tableId, @ModelAttribute("currentItemId") String itemId, RedirectAttributes redirect) {
//        Order order = orderRepository.findBy_id(id);
//        orderRepository.delete(order);
//        String redirectValue = "orders/" +order.getTableId()+ "/tables";
//        redirect.addFlashAttribute("successMessage", "Delete Order Success");
//        return redirectValue;
//    }
//
//    @Override
//    public String paidOrder(String id, @ModelAttribute("currentTableId")  String tableId, @ModelAttribute("currentItemId") String itemId, RedirectAttributes redirect) {
//        Order order = orderRepository.findBy_id(id);
//        order.setPaid(true);
//        orderRepository.save(order);
//        String redirectValue = "orders/" +order.getTableId()+ "/tables";
//        redirect.addFlashAttribute("successMessage", "Set Paid Order Success");
//        return redirectValue;
//    }
//
//    @Override
//    public String unpaidOrder(String id, @ModelAttribute("currentTableId")  String tableId, @ModelAttribute("currentItemId") String itemId, RedirectAttributes redirect) {
//        Order order = orderRepository.findBy_id(id);
//        order.setPaid(false);
//        orderRepository.save(order);
//        String redirectValue = "orders/" +order.getTableId()+ "/tables";
//        redirect.addFlashAttribute("successMessage", "Set Unpaid Order Success");
//        return redirectValue;
//    }
//}
