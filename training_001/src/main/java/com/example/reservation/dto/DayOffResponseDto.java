package com.example.reservation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DayOffResponseDto {
    @JsonProperty("name")
    private String name;
    @JsonProperty("day_off")
    private int day_off;
    @JsonProperty("modified")
    private int modified;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDay_off() {
        return day_off;
    }

    public void setDay_off(int day_off) {
        this.day_off = day_off;
    }

    public int getModified() {
        return modified;
    }

    public void setModified(int modified) {
        this.modified = modified;
    }
}
