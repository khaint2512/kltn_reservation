package com.example.reservation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReservationResponseDto {
    @JsonProperty(value = "state")
    private int state;
    @JsonProperty(value = "reservation_code")
    private String uniqueCode;
    @JsonProperty("created")
    private int created;
    @JsonProperty("modified")
    private int modified;
    @JsonProperty("canceled")
    private int canceled;
    @JsonProperty("uid")
    private String uid;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public int getModified() {
        return modified;
    }

    public void setModified(int modified) {
        this.modified = modified;
    }

    public int getCanceled() {
        return canceled;
    }

    public void setCanceled(int canceled) {
        this.canceled = canceled;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
