package com.example.reservation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReservationDetailsRequestDto {

    @JsonProperty("uid")
    private String uid;
    @JsonProperty("customer_name")
    private String customer_name;
    @JsonProperty("phone_number")
    private String phone_number;
    @JsonProperty("email")
    private String email;
    @JsonProperty("reservation_datetime")
    private int reservation_datetime;
    @JsonProperty("person_number")
    private int person_number;
    @JsonProperty("item_name")
    private String item_name;
    @JsonProperty("item_number")
    private int item_number;
    @JsonProperty("note")
    private String note;
    @JsonProperty("reason")
    private String reason;

    public ReservationDetailsRequestDto(){
    }

    public ReservationDetailsRequestDto(String customer_name, String phone_number, String email, int reservation_datetime, int person_number, String item_name, int item_number) {
        this.customer_name = customer_name;
        this.phone_number = phone_number;
        this.email = email;
        this.reservation_datetime = reservation_datetime;
        this.person_number = person_number;
        this.item_name = item_name;
        this.item_number = item_number;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getReservation_datetime() {
        return reservation_datetime;
    }

    public void setReservation_datetime(int reservation_datetime) {
        this.reservation_datetime = reservation_datetime;
    }

    public int getPerson_number() {
        return person_number;
    }

    public void setPerson_number(int person_number) {
        this.person_number = person_number;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public int getItem_number() {
        return item_number;
    }

    public void setItem_number(int item_number) {
        this.item_number = item_number;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
