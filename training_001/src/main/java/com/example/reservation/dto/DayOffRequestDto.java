package com.example.reservation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DayOffRequestDto {
    @JsonProperty("uid")
    private String uid;
    @JsonProperty("state")
    private int state;
    @JsonProperty("name")
    private String name;
    @JsonProperty("day_off")
    private int day_off;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDay_off() {
        return day_off;
    }

    public void setDay_off(int day_off) {
        this.day_off = day_off;
    }
}
