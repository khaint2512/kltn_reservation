package com.example.reservation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseEntityDto {
    @JsonProperty(value = "table_id")
    private int table_id;
    @JsonProperty(value = "customer_name")
    private int customer_name;
    @JsonProperty(value = "phone_number")
    private int phone_number;
    @JsonProperty(value = "email")
    private int email;
    @JsonProperty(value = "reservation_datetime")
    private int reservation_datetime;
    @JsonProperty(value = "person_number")
    private int person_number;
    @JsonProperty(value = "item_name")
    private int item_name;
    @JsonProperty(value = "item_number")
    private int item_number;

    public int getTable_id() {
        return table_id;
    }

    public void setTable_id(int table_id) {
        this.table_id = table_id;
    }

    public int getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(int customer_name) {
        this.customer_name = customer_name;
    }

    public int getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(int phone_number) {
        this.phone_number = phone_number;
    }

    public int getEmail() {
        return email;
    }

    public void setEmail(int email) {
        this.email = email;
    }

    public int getReservation_datetime() {
        return reservation_datetime;
    }

    public void setReservation_datetime(int reservation_datetime) {
        this.reservation_datetime = reservation_datetime;
    }

    public int getPerson_number() {
        return person_number;
    }

    public void setPerson_number(int person_number) {
        this.person_number = person_number;
    }

    public int getItem_name() {
        return item_name;
    }

    public void setItem_name(int item_name) {
        this.item_name = item_name;
    }

    public int getItem_number() {
        return item_number;
    }

    public void setItem_number(int item_number) {
        this.item_number = item_number;
    }
}
