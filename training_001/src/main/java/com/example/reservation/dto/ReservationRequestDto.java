package com.example.reservation.dto;

import com.example.reservation.model.ReservationOrderDetails;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ReservationRequestDto {

    @JsonProperty("uid")
    private String uid;
    @JsonProperty("version")
    private String version;
    @JsonProperty("code")
    private String code;
    @JsonProperty("id")
    private String id;
    @JsonProperty("shopCode")
    private String shopCode;
    @JsonAlias({"owner_id","ownerId"})
    private String owner_id;
    @JsonAlias({"service_id","serviceId"})
    private String service_id;
    @JsonAlias({"reservation_code","reservationCode"})
    private String unique_code;
    @JsonProperty("shop_id")
    private int shop_id;
    @JsonProperty("table_id")
    private String table_id;
    @JsonProperty("state")
    private int state;
    @JsonProperty("customer_name")
    private String customer_name;
    @JsonProperty("phone_number")
    private String phone_number;
    @JsonProperty("email")
    private String email;
    @JsonProperty("reservation_datetime")
    private int reservation_datetime;
    @JsonProperty("person_number")
    private int person_number;

    @JsonAlias({"reservation_detail_jsonarray", "reservationDetailJsonarray"})
    private List<ReservationOrderDetails> reservationOrderDetails;

    @JsonProperty("note")
    private String note;
    @JsonProperty("reason")
    private String reason;
    @JsonProperty("created")
    private int created;

    public ReservationRequestDto(){
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUnique_code() {
        return unique_code;
    }

    public void setUnique_code(String unique_code) {
        this.unique_code = unique_code;
    }

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getReservation_datetime() {
        return reservation_datetime;
    }

    public void setReservation_datetime(int reservation_datetime) {
        this.reservation_datetime = reservation_datetime;
    }

    public int getPerson_number() {
        return person_number;
    }

    public void setPerson_number(int person_number) {
        this.person_number = person_number;
    }

    public List<ReservationOrderDetails> getReservationOrderDetails() {
        return reservationOrderDetails;
    }

    public void setReservationOrderDetails(List<ReservationOrderDetails> reservationOrderDetails) {
        this.reservationOrderDetails = reservationOrderDetails;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }
}
